import React, { useEffect, useState } from 'react'; // Import React and the necessary hooks first
import Nav from './Nav';
import LocationForm from './LocationForm'; // Import the newly created component here
import AttendeesList from './AttendeesList';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        <LocationForm /> {/* Render the LocationForm component here */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}

export default App;
