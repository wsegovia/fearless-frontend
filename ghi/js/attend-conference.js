window.addEventListener('DOMContentLoaded', async () => {
  const form = document.getElementById('create-attendee-form');
  const selectTag = document.getElementById('conference');
  const loadingIcon = document.getElementById('loading-conference-spinner');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    // Remove the 'd-none' class from the select tag
    selectTag.classList.remove('d-none');

    // Add the 'd-none' class to the loading icon
    loadingIcon.classList.add('d-none');
  }

  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const attendeeData = Object.fromEntries(formData.entries());

    const fetchOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(attendeeData),
    };

    const submitUrl = 'http://localhost:8001/api/attendees/';
    const response = await fetch(submitUrl, fetchOptions);

    if (response.ok) {
      // Handle successful form submission
      form.reset();
      document.getElementById('success-message').classList.remove('d-none');
    } else {
      // Handle error response
      console.error('Form submission failed:', response.statusText);
    }
  });
});
