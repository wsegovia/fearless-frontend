function createCard(name, description, pictureUrl, starts, ends, location) {
  const formattedStarts = formatDate(starts);
  const formattedEnds = formatDate(ends);

  return `
    <div class="card shadow p-3 mb-3">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-3 text-secondary">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${formattedStarts} - ${formattedEnds}</small>
      </div>
    </div>
  `;
}

function formatDate(dateString) {
  const date = new Date(dateString);

  if (isNaN(date)) {
    return 'Invalid Date';
  }

  const options = {
    month: 'numeric',
    day: 'numeric',
    year: '2-digit'
  };

  return date.toLocaleDateString('en-US', options);
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();

    const columnContainers = document.querySelectorAll('.col');

    for (let i = 0; i < data.conferences.length; i++) {
      const conference = data.conferences[i];
      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const name = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const starts = details.conference.starts;
        const ends = details.conference.ends;
        const location = details.conference.location.name; // Add location variable
        const html = createCard(name, description, pictureUrl, starts, ends, location);

        // Add card to respective column container
        const columnContainer = columnContainers[i % columnContainers.length];
        columnContainer.innerHTML += html;
      }
    }
  } catch (error) {
    console.error(error);
    const errorMessage = document.getElementById('error-message');
    errorMessage.innerHTML = `
      <div class="alert alert-danger" role="alert">
        An error occurred: ${error.message}
      </div>
    `;
  }
});
