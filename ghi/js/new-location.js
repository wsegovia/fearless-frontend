// document.addEventListener('DOMContentLoaded', async () => {
//   const apiUrl = 'http://localhost:8000/api/states/';

// try {
//     const response = await fetch(apiUrl);
//   if (!response.ok) {
//       throw new Error('Failed to fetch states');
//   }

//   const data = await response.json();
//   const states = data.states;
//     const selectElement = document.getElementById('state');
//   for (const state of states) {
//     const option = document.createElement('option');
//     option.value = state.abbreviation;
//     option.textContent = state.name;
//       selectElement.appendChild(option);
//   }
// } catch (error) {
//   console.error(error);
//   }
// });

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById('state');

    for (let state of data.states) {
      const option = document.createElement('option');
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      selectTag.appendChild(option);
    }
}
});

window.addEventListener('DOMContentLoaded', async () => {

  // State fetching code, here...

  const formTag = document.getElementById('create-location-form');

  formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      }
    } catch (error) {
      console.log(error)
    }

  })});
